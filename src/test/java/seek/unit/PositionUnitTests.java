package seek.unit;

import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import seek.domain.dto.PositionDto;
import seek.domain.entity.Position;

@MicronautTest(environments = {"test"})
class PositionUnitTests {

    @Test
    void testCreatePosition() {
        final Position position = new Position();
        Assertions.assertNotNull(position.getId());
    }

    @Test
    void testCreatePositionDto() {
        final Position position = new Position();
        final PositionDto dto = new PositionDto(position);
        Assertions.assertNotNull(dto.getId());
    }
}
